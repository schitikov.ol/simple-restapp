package com.linefight.rooxrest.controller;

import com.linefight.rooxrest.entity.Customer;
import com.linefight.rooxrest.entity.PartnerMapping;
import com.linefight.rooxrest.service.CustomerService;
import com.linefight.rooxrest.service.PartnerMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("customer")
public class CustomerRestController {

    private CustomerService customerService;
    private PartnerMappingService partnerMappingService;

    @Autowired
    public CustomerRestController(CustomerService customerService, PartnerMappingService partnerMappingService) {
        this.customerService = customerService;
        this.partnerMappingService = partnerMappingService;
    }

    @GetMapping("{customerId}")
    public Customer customer(@PathVariable("customerId") String customerId) {

        if ("@me".equals(customerId)) {
            return (Customer) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }

        return customerService.findCustomerByIdAuthorized(Long.parseLong(customerId));
    }

    @GetMapping("{customerId}/partner_mapping")
    public List<PartnerMapping> partnerMappings(@PathVariable("customerId") String customerId) {
        return partnerMappingService.findPartnerMappingsByCustomerId(customer(customerId).getId());
    }

    @GetMapping("{customerId}/partner_mapping/{partnerMappingId}")
    public PartnerMapping partnerMapping(@PathVariable("customerId") String customerId, @PathVariable("partnerMappingId") long partnerMappingId) {
        return partnerMappingService.findPartnerMappingByIdAndCustomerId(partnerMappingId, customer(customerId).getId());
    }

//    @PostMapping("{customerId}/partner_mapping")
//    public PartnerMapping createPartnerMapping(@PathVariable("customerId") String customerId, @RequestBody PartnerMapping mappingToCreate) {
//
//    }

    private static class PartnerMappingForm {
        private long partnerId;
        private String accountFirstName;
        private String accountLastName;
        private String accountMidName;
        private byte[] accountAvatar;

        public long getPartnerId() {
            return partnerId;
        }

        public void setPartnerId(long partnerId) {
            this.partnerId = partnerId;
        }

        public String getAccountFirstName() {
            return accountFirstName;
        }

        public void setAccountFirstName(String accountFirstName) {
            this.accountFirstName = accountFirstName;
        }

        public String getAccountLastName() {
            return accountLastName;
        }

        public void setAccountLastName(String accountLastName) {
            this.accountLastName = accountLastName;
        }

        public String getAccountMidName() {
            return accountMidName;
        }

        public void setAccountMidName(String accountMidName) {
            this.accountMidName = accountMidName;
        }

        public byte[] getAccountAvatar() {
            return accountAvatar;
        }

        public void setAccountAvatar(byte[] accountAvatar) {
            this.accountAvatar = accountAvatar;
        }
    }
}
