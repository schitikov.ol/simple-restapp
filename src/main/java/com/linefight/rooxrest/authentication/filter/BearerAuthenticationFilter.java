package com.linefight.rooxrest.authentication.filter;

import com.linefight.rooxrest.authentication.token.BearerAuthenticationToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BearerAuthenticationFilter implements Filter {

    private static final Logger log = LogManager.getLogger();
    private AuthenticationManager authenticationManager;
    private AuthenticationEntryPoint authenticationEntryPoint = new Http403ForbiddenEntryPoint();

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) req;
        final HttpServletResponse response = (HttpServletResponse) res;

        String authorizationHeader = request.getHeader("Authorization");

        boolean success = false;

        if (authorizationHeader == null) {
            log.warn("Request doesn't have Authorization header.");
        } else {
            String[] tokens = authorizationHeader.trim().split(" ");
            if (tokens.length != 2 || !"Bearer".equals(tokens[0])) {
                log.warn("Authorization header consists of more than 2 tokens or the first token isn't Bearer.");
            } else {
                long userId;
                try {
                    userId = Long.parseLong(tokens[1]);
                    Authentication authentication = authenticationManager.authenticate(new BearerAuthenticationToken(userId));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    log.debug("Successful login with token {}", authentication);
                    success = true;
                } catch (NumberFormatException ex) {
                    log.warn("Can't parse user ID token.");
                }
            }
        }

        if (!success) {
            log.warn("Authentication failed, clear Security Context");
            SecurityContextHolder.clearContext();
            //           authenticationEntryPoint.commence(request, response, new BadCredentialsException("Bad credentials."));
            return;
        }

        chain.doFilter(req, res);
    }

    @Override
    public void destroy() {

    }
}
