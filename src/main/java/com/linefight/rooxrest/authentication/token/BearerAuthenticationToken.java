package com.linefight.rooxrest.authentication.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;

public class BearerAuthenticationToken extends AbstractAuthenticationToken {

    private final long userId;

    public BearerAuthenticationToken(long userId) {
        super(Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));

        this.userId = userId;
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return userId;
    }

    @Override
    public String toString() {
        return "BearerAuthenticationToken{" +
                "userId=" + userId +
                '}';
    }
}
