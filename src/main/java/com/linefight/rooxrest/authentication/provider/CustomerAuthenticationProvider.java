package com.linefight.rooxrest.authentication.provider;

import com.linefight.rooxrest.authentication.CustomerAuthentication;
import com.linefight.rooxrest.authentication.token.BearerAuthenticationToken;
import com.linefight.rooxrest.service.CustomerService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

public class CustomerAuthenticationProvider implements AuthenticationProvider {

    private CustomerService customerService;

    public CustomerAuthenticationProvider(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public CustomerAuthentication authenticate(Authentication authentication) throws AuthenticationException {
        CustomerAuthentication customerAuthentication = new CustomerAuthentication(customerService.findCustomerById((Long) authentication.getPrincipal()));
        customerAuthentication.setAuthenticated(true);
        return customerAuthentication;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return BearerAuthenticationToken.class.equals(authentication);
    }
}
