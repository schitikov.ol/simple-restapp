package com.linefight.rooxrest.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlSchemaType;

@Entity
@Table(name = "partner_mapping", uniqueConstraints = @UniqueConstraint(name = "partner_id_account_id", columnNames = {"partner_id", "account_id"}))
@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.NONE,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class PartnerMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ID")
    private long id;

    @Column(name = "customer_id", nullable = false)
    private long customerId;

    @Column(name = "partner_id", nullable = false)
    private long partnerId;

    @Column(name = "account_id", nullable = false)
    private long accountId;

    @Column(name = "account_first_name", nullable = false)
    private String accountFirstName;

    @Column(name = "account_last_name", nullable = false)
    private String accountLastName;

    @Column(name = "account_mid_name")
    private String accountMidName;

    @Lob
    @XmlSchemaType(name = "base64Binary")
    @Column(name = "account_avatar")
    @Basic(fetch = FetchType.LAZY)
    private byte[] accountAvatar;

    @JsonProperty
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @JsonProperty
    public long getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(long partnerId) {
        this.partnerId = partnerId;
    }

    @JsonProperty
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @JsonProperty
    public String getAccountFirstName() {
        return accountFirstName;
    }

    public void setAccountFirstName(String accountFirstName) {
        this.accountFirstName = accountFirstName;
    }

    @JsonProperty
    public String getAccountLastName() {
        return accountLastName;
    }

    public void setAccountLastName(String accountLastName) {
        this.accountLastName = accountLastName;
    }

    @JsonProperty
    public String getAccountMidName() {
        return accountMidName;
    }

    public void setAccountMidName(String accountMidName) {
        this.accountMidName = accountMidName;
    }

    public byte[] getAccountAvatar() {
        return accountAvatar;
    }

    public void setAccountAvatar(byte[] accountAvatar) {
        this.accountAvatar = accountAvatar;
    }
}
