package com.linefight.rooxrest.service;

import com.linefight.rooxrest.entity.Customer;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;

public interface CustomerService {

    Customer findCustomerById(long customerId);

    @PreAuthorize("(authentication.principal.id == #customerId) or hasAuthority('ADMIN')")
    Customer findCustomerByIdAuthorized(@P("customerId") long customerId);
}
