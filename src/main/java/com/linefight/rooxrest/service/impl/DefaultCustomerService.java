package com.linefight.rooxrest.service.impl;

import com.linefight.rooxrest.entity.Customer;
import com.linefight.rooxrest.repository.CustomerRepository;
import com.linefight.rooxrest.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class DefaultCustomerService implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public DefaultCustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Customer findCustomerById(long customerId) {
        return customerRepository.findById(customerId).get();
    }

    @Override
    @Transactional(readOnly = true)
    public Customer findCustomerByIdAuthorized(long customerId) {
        return findCustomerById(customerId);
    }
}
