package com.linefight.rooxrest.service.impl;

import com.linefight.rooxrest.entity.PartnerMapping;
import com.linefight.rooxrest.repository.PartnerMappingRepository;
import com.linefight.rooxrest.service.PartnerMappingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DefaultPartnerMappingService implements PartnerMappingService {

    private PartnerMappingRepository partnerMappingRepository;

    @Autowired
    public DefaultPartnerMappingService(PartnerMappingRepository partnerMappingRepository) {
        this.partnerMappingRepository = partnerMappingRepository;
    }


    @Override
    @Transactional(readOnly = true)
    public List<PartnerMapping> findPartnerMappingsByCustomerId(long customerId) {
        return partnerMappingRepository.findPartnerMappingsByCustomerId(customerId);
    }

    @Override
    @Transactional(readOnly = true)
    public PartnerMapping findPartnerMappingByIdAndCustomerId(long partnerMappingId, long customerId) {
        return partnerMappingRepository.findPartnerMappingByIdAndCustomerId(partnerMappingId, customerId);
    }
}
