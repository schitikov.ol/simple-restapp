package com.linefight.rooxrest.service;

import com.linefight.rooxrest.entity.PartnerMapping;

import java.util.List;

public interface PartnerMappingService {
    List<PartnerMapping> findPartnerMappingsByCustomerId(long customerId);

    PartnerMapping findPartnerMappingByIdAndCustomerId(long partnerMappingId, long customerId);
}
