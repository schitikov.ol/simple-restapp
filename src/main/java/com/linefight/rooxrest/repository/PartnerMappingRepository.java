package com.linefight.rooxrest.repository;

import com.linefight.rooxrest.entity.PartnerMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PartnerMappingRepository extends JpaRepository<PartnerMapping, Long> {
    List<PartnerMapping> findPartnerMappingsByCustomerId(long customerId);

    PartnerMapping findPartnerMappingByIdAndCustomerId(long partnerMappingId, long customerId);

    @Query(value = "call NEXTVAL('SEQ_ID')", nativeQuery = true)
    long nextId();
}
