package com.linefight.rooxrest.repository;

import com.linefight.rooxrest.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
