create table customer (
  id              bigint             not null primary key,
  login           varchar(60) unique not null,
  hashed_password binary(60)         not null,
  balance         decimal,
  first_name      varchar(60)        not null,
  last_name       varchar(60)        not null,
  mid_name        varchar(60),
  status          boolean            not null
);

create table partner_mapping (
  id                 bigint      not null primary key,
  customer_id        bigint      not null,
  partner_id         bigint      not null,
  account_id         bigint      not null,
  account_first_name varchar(60) not null,
  account_last_name  varchar(60) not null,
  account_mid_name   varchar(60),
  account_avatar     blob,
  constraint partner_mapping_customer_fk foreign key (customer_id) references customer (id) on delete cascade,
  constraint partner_id_account_id unique (partner_id, account_id)
);

create sequence SEQ_ID
  start with 1
  increment by 1
  minvalue 1
  no maxvalue
  no cycle
  cache 50;